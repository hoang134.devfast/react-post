import React,{Suspense} from 'react';
import { BrowserRouter ,Route, Routes } from 'react-router-dom'
import NotFound from './component/NotFound'

import 'bootstrap/dist/css/bootstrap.min.css';
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import {  faCoffee,
          faArrowLeft,
          faSave,
          faLock,
          faEnvelope,
          faUser,
          faPlus,
          faChevronLeft,
          faTachometerAlt, 
          faUsers,
          faClipboard,
          faPassport,
          faCaretSquareDown,
          faCogs,
          faEdit,
          faTrashAlt,
          faPencilAlt,
          faInfo,
          faMapPin,
          faArchive,
          faBoxes,
          faDatabase,
          faBookOpen,
          faEraser,
          faSearch,
          faBezierCurve,
          faDirections,
          faStar,
          faMapMarker,
          faChevronRight
} from '@fortawesome/free-solid-svg-icons'

library.add(
  fab,
  faCoffee,
  faArrowLeft,
  faSave,
  faLock,
  faDirections,
  faBezierCurve,
  faEnvelope,
  faUser,
  faPlus,
  faChevronLeft,
  faTachometerAlt,
  faUsers,
  faClipboard,
  faPassport,
  faCaretSquareDown,
  faCogs,
  faEdit,
  faTrashAlt,
  faPencilAlt,
  faInfo,
  faMapPin,
  faArchive,
  faBoxes,
  faDatabase,
  faBookOpen,
  faEraser,
  faSearch,
  faStar,
  faMapMarker,
  faChevronRight
  )


const Post = React.lazy(() => import('./module/post')) 
//import Post from './module/post'

function App() {
  return (
     <Suspense fallback={<div>Loading ...</div>}>
      <BrowserRouter>
    
        <div className="body-site">
          <Routes>
            <Route path="posts/*" element={<Post/>} />
            <Route path="*" element={<NotFound/>} />
          </Routes>
        </div>
      </BrowserRouter>
    </Suspense>
  );
}

export default App;
