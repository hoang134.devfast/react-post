export const LIST_CATEGORY = [
  { id: 1, name: 'SEO', categoryUrl: '/seo/' },
  { id: 2, name: 'アクセス解析', categoryUrl: '/access-kaiseki/' },
  { id: 3, name: 'コンテンツマーケティング', categoryUrl: '/content/' },
  { id: 4, name: 'サイト改善 / 制作', categoryUrl: '/sitekaizenn-seisaku/' },
  { id: 5, name: 'WEB広告', categoryUrl: '/web/' },
  { id: 6, name: 'WEBマーケティング', categoryUrl: '/webmarketing/' },
  { id: 7, name: 'セミナー', categoryUrl: '/seminar/' },
  { id: 8, name: '導入実績', categoryUrl: '/result/' },
  { id: 9, name: 'お知らせ', categoryUrl: '/notice/' },
  { id: 10, name: 'SNS', categoryUrl: '/sns/' }
]
