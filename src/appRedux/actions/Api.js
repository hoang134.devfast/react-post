import * as ActionTypes from "../../constants/ActionTypes";



export const getRefreshTokenSuccess = (payload) => {
  return {
    type: ActionTypes.REFRESH_TOKEN_SUCCESS,
    payload: payload,
  };
};

// export const profileFetchStart = () => {
//   return {
//     type: ActionTypes.PROFILE_FETCH_START,
//   };
// };

// export const profileFetchSuccess = (profile) => {
//   // console.log("action profileFetchSuccess", profile)
//   return {
//     type: ActionTypes.PROFILE_FETCH_SUCCESS,
//     payload: profile,
//   };
// };

// export const profileFetchError = (error) => {
//   return {
//     type: ActionTypes.PROFILE_FETCH_ERROR,
//     payload: error,
//   };
// };
