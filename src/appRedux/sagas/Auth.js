import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import axios from "axios";
import { authApi } from "../../constants/ApiServer";
import {
  SIGNIN_USER,
  SIGNOUT_USER,
  SIGNUP_USER,
} from "../../constants/ActionTypes";
import {
  showAuthMessage,
  userSignInSuccess,
  userSignOutSuccess,
  userSignUpSuccess,
  userSignUp,
} from "../../appRedux/actions/Auth";
import { message } from "antd";


const signInUserWithOwnAddresRequest = (payload) =>{

  return axios.post(authApi.login, payload)
  .then(function (response) {
    console.log(response)
    return response.data;
  });
 
}
const createUserRequest = (payload) =>{
  console.log("createUserRequest",payload)
  return axios.post(authApi.register, payload)
  .then(function (response) {
    return response.data;
  });
 
}

// const signOutRequest = async () =>
//   await auth
//     .signOut()
//     .then((authUser) => authUser)
//     .catch((error) => error);

// const signInUserWithGoogleRequest = async () =>
//   await auth
//     .signInWithPopup(googleAuthProvider)
//     .then((authUser) => authUser)
//     .catch((error) => error);

// const signInUserWithFacebookRequest = async () =>
//   await auth
//     .signInWithPopup(facebookAuthProvider)
//     .then((authUser) => authUser)
//     .catch((error) => error);

function* createUser({ payload }) {
  console.log("createUser payload",payload)
  try {
    const signUpUser = yield call(createUserRequest,payload);
    // console.log("signUpUser",signUpUser)
    if (signUpUser && signUpUser.accessToken !== null) {
      localStorage.setItem("token", JSON.stringify(signUpUser) );
      message.info("Register success!")
      yield put(userSignInSuccess(signInUser));
    }
  } catch (error) {
    console.log(error)
    if (error.response.status === 400) {
      console.log("dk khong thanh cong")
      message.error("Register failed")
    }
   
    // yield put(showAuthMessage("Register failed"));
  }
}
function getCurrentTimeUTC()
{
    //RETURN:
    //      = number of milliseconds between current UTC time and midnight of January 1, 1970
    var tmLoc = new Date();
    //The offset is in minutes -- convert it to ms
    return tmLoc.getTime() + tmLoc.getTimezoneOffset() * 60000;
}
function* signInUserWithOwnAddres({ payload }) {
  try {
    const signInUser = yield call(signInUserWithOwnAddresRequest,payload);
    // const signInUser = "test";
    console.log("signInUser",signInUser);
    if (signInUser && signInUser.accessToken !== null) {
      localStorage.setItem("token", JSON.stringify(signInUser) );
      message.info("Login success! Wellcome back " + signInUser.userName)
      yield put(userSignInSuccess(signInUser));
    }
  } catch (error) {
    if (error.response.status === 401) {
      message.info("New user! Please wait for create account")
      console.log(payload)
      var ticks = getCurrentTimeUTC();
      const signUpUserRequest = {...payload, userName:"user"+ticks}
     
      yield put(userSignUp(signUpUserRequest));
    }
  }
}

function* signOut() {
  try {
    // const signOutUser = yield call(signOutRequest);
    const signOutUser = undefined;
    if (signOutUser === undefined) {
      localStorage.removeItem("token");
      yield put(userSignOutSuccess(signOutUser));
    } else {
      yield put(showAuthMessage(signOutUser.message));
    }
  } catch (error) {
    yield put(showAuthMessage(error));
  }
}

export function* createUserAccount() {
  yield takeEvery(SIGNUP_USER, createUser);
}

export function* signInUser() {
  yield takeEvery(SIGNIN_USER, signInUserWithOwnAddres);
}

export function* signOutUser() {
  yield takeEvery(SIGNOUT_USER, signOut);
}

export default function* rootSaga() {
  yield all([
    fork(signInUser),
    fork(createUserAccount),
    fork(signOutUser),
  ]);
}
