import { all, call, fork, put, takeEvery,takeLatest } from "redux-saga/effects";
import axios from "axios";
import * as ApiCall from "../../constants/ApiServer";
import * as action from "../../appRedux/actions/Api";

import { message } from "antd";
import { userSignOut } from "../actions";
import store from "../store";


function requestToken(refreshToken) {
  // console.log("refreshToken start")
  // const refreshToken = localStorage.getItem('refreshToken');
  const refreshTokenURL = ApiCall.authApi.refreshToken;
  return axios.post(refreshTokenURL, { refreshToken: refreshToken })
}

function* getf5TokenFailure() {
  yield call(userSignOut)
}

function handleCommonError(error, onError) {
  console.log(error)
  if (error.response && error.response.data) {
    // console.log(error.response)

    if (error.response.status === 401) {
      const token = localStorage.getItem('token');
      const refreshToken = localStorage.getItem('refreshToken');
      if (token === null || refreshToken === null) {
        message.info("You need login to use this feature!");
        store.dispatch(userSignOut())
        return;
      }
      requestToken(refreshToken).then((response) => {
        // console.log(response)
        const data = response.data
        if (data && data.success) {
          localStorage.setItem("token", data.accessToken);
          error.config.headers.Authorization = `Bearer ${data.accessToken}`
          // console.log("recall failed with config", error.config)
          // axios.request(error.config)
          store.dispatch(action.getRefreshTokenSuccess(data.accessToken))
        }

      }).catch(function (error) {
        message.error("Your token has expired,Please login again!");
        store.dispatch(userSignOut())
      });
    }
    else {
      message.error('System Error!');
    }
  }
  else {
    message.error('System Error!');
  }
}

// const getProfileRequest = () => {
//   return axios.get(ApiCall.getProfileApi, ApiCall.getConfigGetWithToken())
//     .then(function (response) {
//       // console.log(response)
//       return response
//     })
//     .catch(function (error) {
//       return { error: error }
//     });
// }

// function* getProfile() {
//   try {
//     const response = yield call(
//       getProfileRequest
//     );
//     if (response.error) {
//       handleCommonError(response.error)
//     } else {
//       // console.log("getProfile ok", response.data)
//       localStorage.setItem("profile", JSON.stringify(response.data.profile));
//       yield put(action.profileFetchSuccess(response.data.profile));
//     }
//   } catch (error) {
//     yield put(action.profileFetchError(error));
//   }
// }



export function* getApi() {

}


export default function* rootSaga() {
  yield all([
    fork(getApi)
  ]);
}

/////NO SAGA ------------------------------------------------

export const getMystery = ( onSuccess) => {
  return axios.get(ApiCall.mysteryApi.get,{
    // ...ApiCall.getConfigGetWithToken(),
    // params: payload,
  })
    .then(function (response) {
      // console.log(response)
      if (response.data) {
        if (onSuccess) { onSuccess(response.data.data) }
      } else {
        
      }
      return response
    })
    .catch(function (error) {
      handleCommonError(error, getf5TokenFailure)
    });;
}

export const holdMystery = ( onSuccess) => {
  return axios.post(ApiCall.mysteryApi.hold,null, ApiCall.getConfigGetWithToken())
    .then(function (response) {
      // console.log("holdMystery ", response)
      if (response.data) {
        if (onSuccess) { onSuccess(response.data.data) }
      } else {
        
      }
      return response
    })
    .catch(function (error) {
      handleCommonError(error, getf5TokenFailure)
    });;
}


export const openMystery = (payload, onSuccess) => {
  // console.log(payload)
  const url = ApiCall.mysteryApi.open + "?txId="+payload.txId
  return axios.post(url, null, ApiCall.getConfigGetWithToken())
    .then(function (response) {
      // console.log("openMystery ", response)
      if (response.data) {
        if (onSuccess) { onSuccess(response.data.data) }
      } else {
        
      }
      return response
    })
    .catch(function (error) {
      handleCommonError(error, getf5TokenFailure)
    });;
}
export const changeHorseName = (payload, onSuccess) => {
  console.log("changeHorseName ",payload)
  return axios.post(ApiCall.stableApi.setName, null, {
    ...ApiCall.getConfigGetWithToken(),
    params: payload,
  })
    .then(function (response) {
      console.log("changeHorseName ", response)
      if (response.data) {
        const data = response.data
       
        if ( data.code === "OK" && onSuccess) { onSuccess(data.data) }

        else {
          message.error(data.msg)
        }
      } else {
        
      }
      return response
    })
    .catch(function (error) {
      handleCommonError(error, getf5TokenFailure)
    });;
}

export const getHorseProfile = (payload, onSuccess) => {
  // console.log(payload)
  const url = ApiCall.stableApi.profile + "?id="+payload.id
  return axios.get(url, ApiCall.getConfigGetWithToken())
    .then(function (response) {
      if (response.data) {
        // console.log("getHorseProfile ", response.data)
        if (onSuccess && response.data.data) { onSuccess(response.data.data) }
      } else {
        
      }
      return response
    })
    .catch(function (error) {
      handleCommonError(error, getf5TokenFailure)
    });;
}

export const getBloodline = ( onSuccess) => {
  return axios.get(ApiCall.stableApi.Bloodline,{
    ...ApiCall.getConfigGetWithToken(),
    // params: payload,
  })
    .then(function (response) {
      // console.log(response)
      if (response.data) {
        if (onSuccess) { onSuccess(response.data.data) }
      } else {
        
      }
      return response
    })
    .catch(function (error) {
      handleCommonError(error, getf5TokenFailure)
    });;
}

export const getGender = ( onSuccess) => {
  return axios.get(ApiCall.stableApi.Gender,{
    ...ApiCall.getConfigGetWithToken(),
    // params: payload,
  })
    .then(function (response) {
      // console.log(response)
      if (response.data) {
        if (onSuccess) { onSuccess(response.data.data) }
      } else {
        
      }
      return response
    })
    .catch(function (error) {
      handleCommonError(error, getf5TokenFailure)
    });;
}

export const getGenotype = ( onSuccess) => {
  return axios.get(ApiCall.stableApi.Genotype,{
    ...ApiCall.getConfigGetWithToken(),
    // params: payload,
  })
    .then(function (response) {
      // console.log(response)
      if (response.data) {
        if (onSuccess) { onSuccess(response.data.data) }
      } else {
        
      }
      return response
    })
    .catch(function (error) {
      handleCommonError(error, getf5TokenFailure)
    });;
}

export const getSort = ( onSuccess) => {
  return axios.get(ApiCall.stableApi.Sort,{
    ...ApiCall.getConfigGetWithToken(),
    // params: payload,
  })
    .then(function (response) {
      // console.log(response)
      if (response.data) {
        if (onSuccess) { onSuccess(response.data.data) }
      } else {
        
      }
      return response
    })
    .catch(function (error) {
      handleCommonError(error, getf5TokenFailure)
    });;
}
const encodeArrayParam = (paramName,array)=> {
  console.log(paramName, array)
  var arrayBloodline = paramName + array.join('&' + paramName);
  return arrayBloodline
}
export const getListHorse = ( payload,onSuccess) => {
  console.log("payload",payload)
  let stringParamCustom = ""
  if (payload?.bloodline?.length >0) {
    stringParamCustom += encodeArrayParam("bloodline=",payload.bloodline)
    payload.bloodline = null // set = null for not effect other param not use custom encode default by axios
    console.log(stringParamCustom)
  }
  if (payload?.gender?.length >0) {
    stringParamCustom += "&" + encodeArrayParam("gender=",payload.gender)
    payload.gender = null // set = null for not effect other param not use custom encode default by axios
    console.log(stringParamCustom)
  }
  if (payload?.genotype?.length >0) {
    stringParamCustom +="&" +  encodeArrayParam("genotype=",payload.genotype)
    payload.genotype = null // set = null for not effect other param not use custom encode default by axios
    console.log(stringParamCustom)
  }
  return axios.get(ApiCall.stableApi.list+"?"+ stringParamCustom,{
    ...ApiCall.getConfigGetWithToken(),
    params: payload,
  })
    .then(function (response) {
      // console.log(response)
      if (response.data) {
        if (onSuccess) { onSuccess(response.data.data) }
      } else {
        
      }
      return response
    })
    .catch(function (error) {
      handleCommonError(error, getf5TokenFailure)
    });;
}