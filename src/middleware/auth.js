/*
 * Copyright 2021 DevFast Limited. All rights reserved.
 * Email: tech@devfast.us .
 */

// import CONFIG from '~/plugins/dd-api-manager/config'
import CheetahAxios from '../cheetah-core/cheetah-axios/cheetah-axios'
import { Redirect } from 'react-router';

export function CheckLogin() {
  const cheetah = new CheetahAxios()
  cheetah.setType('')

  const authToken = cheetah.getAuthToken(cheetah.getCookieTokenName())
  if (!authToken) {
    return false
  }

  // Update cookies
  cheetah.setAuthToken(authToken)

  // app.$cheetahAxios.getUserInfo().then((res) => {
  //   store.dispatch('user/setUser', res.data)

  //   if (store.getters['user/isAdminSystem']) {
  //     app.$cheetahAxios.clearAuthToken()
  //     return redirect('/login')
  //   }
  // }).catch(() => {
  //   app.$cheetahAxios.clearAuthToken()
  //   return redirect('/login')
  // })

  // app.$cheetahAxios.getSystemSetting().then((res) => {
  //   store.dispatch('system-setting/setSystemSetting', res.data)
  // }).catch(() => {
  //   app.$cheetahAxios.clearAuthToken()
  //   return redirect('/login')
  // })
}
