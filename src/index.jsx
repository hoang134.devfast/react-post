import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Layout from "./component/Layout.jsx";

ReactDOM.render(
  <React.StrictMode>
    <Layout children={App} />
    <App></App>
      {/* <App /> */}
  </React.StrictMode>,
  document.getElementById('root')
);

