/*
 * Copyright 2021 DevFast Limited. All rights reserved.
 * Email: tech@devfast.us .
 */

// const LocalStoreage = {
//   methods: {
    /**
    * Set the value of the specified local storage item
    */
     export function setLocalStorage (keyname, value) {
      localStorage.setItem(keyname, value)
    }

    /**
     * Get the value of the specified local storage item
     */
     export function getLocalStorage (keyname) {
      return localStorage.getItem(keyname)
    }

    /**
     * Remove the the specified local storage item
     */
     export function removeLocalStorage (keyname) {
      localStorage.removeItem(keyname)
    }

    /**
     * Remove all local storage items
     */
     export function clearLocalStorage () {
      localStorage.clear()
    }
//   }
// }

// export default LocalStoreage
