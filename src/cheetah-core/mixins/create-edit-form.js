/*
 * Copyright 2021 DevFast Limited. All rights reserved.
 * Email: tech@devfast.us .
 */

const EVENT_MODIFY = 'modify'

const CreateEditForm = {
  props: {
    /**
     * Model id
     */
    id: {
      type: Number,
      default: 0
    }
  },

  data () {
    return {
      loading: false
    }
  },

  methods: {
    /**
     * Get item detail
     *
     * @param {Number} id
     */
    getDetail (id) {
      this.loading = true

      if (id) {
        const action = this.model.detailAction

        this.$cheetahAxios[action]({ id })
          .then((res) => {
            this.setModel(res.data)
          })
          .catch((err) => {
            console.error(err)

            this.$toast.error(
              this.$t('messages.error.failed_to_get', { name: this.modelType })
            )
          }).finally(() => {
            this.loading = false
          })
      } else {
        this.setModel()
        this.loading = false
      }
    },

    /**
     * Event trigger on Submit
     */
    async onSubmit () {
      this.loading = true

      try {
        const formData = this.model.getFormData()
        const action = this.id ? this.model.updateAction : this.model.createAction

        await this.$cheetahAxios[action](formData)

        this.$toast.success(
          this.$t(
            this.id ? 'messages.information.updated' : 'messages.information.created',
            { name: this.modelType }
          )
        )

        this.$emit(EVENT_MODIFY)
      } catch (err) {
        console.error(err)

        // const errors = err?.response?.data?.errors || null
        // const message = err?.response?.data?.messages || ''

        // if (Array.isArray(errors.email) && errors.email.length) {
        //   this.$toast.error(errors.email[0])
        // } if (message) {
        //   this.$toast.error(message)
        // } else {
        //   this.$toast.error(
        //     this.$t(
        //       this.id ? 'messages.error.failed_to_update' : 'messages.error.failed_to_create',
        //       { name: this.modelType }
        //     )
        //   )
        // }
      } finally {
        this.loading = false
      }
    }

  }
}

export default CreateEditForm
