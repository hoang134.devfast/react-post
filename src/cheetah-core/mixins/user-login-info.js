/*
 * Copyright 2021 DevFast Limited. All rights reserved.
 * Email: tech@devfast.us .
 */
import * as LocalStoreage from './local-storeage'
const USER = 'user'

// const UserLoginInfo = {
//   methods: {
    /**
    * Set User login from local storage
    */
    export function setUserLogin (user) {
      LocalStoreage.setLocalStorage(USER, JSON.stringify(user))
    }

    /**
     * Get User login from local storage
     */
     export function getUserLogin () {
      return JSON.parse(LocalStoreage.getLocalStorage(USER))
    }

    /**
     * Remove User login from local storage
     */
     export function removeUserLogin () {
      LocalStoreage.removeLocalStorage(USER)
    }
//   }
// }

// export default UserLoginInfo
