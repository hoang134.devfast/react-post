import axiosClient from "./axiosClient";

const userApi = {
  getAll: (params) => {
    const url = '/users';
    return axiosClient.get(url, { params });
  },

  getUserById: (id) => {
    const url = `/users/show/${id}`;
    return axiosClient.get(url);
  },

  createUser: (data) => {
    const url = '/auth/register';
    return axiosClient.post(url, data );
  },

  updateUser: (id,data) => {
    const url = `/users/update/${id}`;
    return axiosClient.put(url, data );
  },

  updateStatus: (id,data) => {
    const url = `/users/updateStatus/${id}`;
    return axiosClient.put(url, data );
  },
  
  delete: (id) => {
    const url = `/users/destroy/${id}`;
    return axiosClient.delete(url);
  },
}

export default userApi;