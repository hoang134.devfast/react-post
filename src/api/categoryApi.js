import axiosClient from "./axiosClient";

const categoryApi = {
  getAll: (params) => {
    const url = '/categories';
    return axiosClient.get(url, { params });
  },

  deleteCategory: (id) => {
    const url = `/categories/destroy/${id}`;
    return axiosClient.delete(url);
  },

  createCategory: (data) => {
    const url = '/categories/store';
    return axiosClient.post(url, data );
  },

  updateCategory: (id,data) => {
    const url = `/categories/update/${id}`;
    return axiosClient.put(url, data );
  },

  updateStatusCate: (id,data) => {
    const url = `/categories/updateStatus/${id}`;
    return axiosClient.put(url, data );
  },

  getCategory: (id) => {
    const url = `/categories/show/${id}`;
    return axiosClient.get(url );
  },
}

export default categoryApi;
