import axiosClient from "./axiosClient";
var baseApiBreeding = process.env.REACT_APP_BASE_API_BREDING

axiosClient.defaults.baseURL= baseApiBreeding
const breedingApi = {

    /**
     * Horse Breeding List by User
     * @param {*} params 
     * @returns 
     */
    horseListByUser: (params) => {
        const url = '/api/Breeding/HorseListByUser';
        return axiosClient.get(url, { params });
    },

    /**
     * Rent Horse in StudFarm
     * @param {*} params 
     * @returns 
     */
    rentHorse: (params) => {
        const url = '/api/Breeding/RentHorse';
        return axiosClient.post(url, { params });
    },

    /**
     * Get List Config Fee Breeding
     * @param {*} params 
     * @returns 
     */
    configFee: (params) => {
        const url = '/api/ConfigFee/ConfigFee';
        return axiosClient.get(url, { params });
    },
    
    /**
     * Horse List in StudFarm
     * @param {*} params 
     * @returns 
     */
    horseList: (params) => {
        const url = '/api/StudFarm/HorseList';
        return axiosClient.get(url, { params });
    },
    
    /**
     * Horse Detail in StudFarm
     * @param {*} params 
     * @returns 
     */
    HorseDetail: (params) => {
        const url = '/api/StudFarm/HorseDetail';
        return axiosClient.get(url, { params });
    },
    
    /**
     * Horse List in StudFarm by User
     * @param {*} params 
     * @returns 
     */
    HorseListByUser: (params) => {
        const url = '/api/StudFarm/HorseListByUser';
        return axiosClient.get(url, { params });
    },

    /**
     * Horse rental to StudFarm
     * @param {*} params 
     * @returns 
     */
    PutHorse: (params) => {
        const url = '/api/StudFarm/PutHorse';
        return axiosClient.post(url, { params });
    }
}

export default breedingApi;