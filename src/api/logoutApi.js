import axiosClient from "./axiosClient";

const LogoutApi = {
    logout: (data) => {
        const url = '/auth/logout';
        return axiosClient.get(url);
      },
}

export default LogoutApi