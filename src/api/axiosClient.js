import axios from 'axios';
import queryString from 'query-string';
import {toast} from 'react-toastify'

// console.log(' process.env.REACT_APP_API_URL',process.env.customKey)
var token = "eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiMTIzIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy91c2VyZGF0YSI6IjEyMyIsImh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwOC8wNi9pZGVudGl0eS9jbGFpbXMvcm9sZSI6IlVzZXIiLCJleHAiOjE2MzYwMzgzMDksImlzcyI6Ik1hcmtldFBsYWNlLkFQSSIsImF1ZCI6Ik1hcmtldFBsYWNlLkNsaWVudCJ9.YymaVgMH3ROlxWklMNMBR4pTos7PNw9EmLBKTPNR5F4"
const axiosClient = axios.create({
  headers: {
    'content-type': 'application/json',
    'Content-Type': 'multipart/form-data',
    'Authorization': 'Bearer ' + token
  },
  paramsSerializer: params => queryString.stringify(params),
});

// axiosClient.defaults.headers.common['Authorization'] = AUTH_TOKEN;

axiosClient.interceptors.response.use((response) => {
  if (response && response.data) {
    return response.data;
  }

  return response;
}, (error) => {
  // Handle errors
  toast.error('Error')
  throw error;
});

export default axiosClient;