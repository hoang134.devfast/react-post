import axiosClient from "./axiosClient";

const settingApi = {
  getAll: (params) => {
    const url = '/settings';
    return axiosClient.get(url, { params });
  },

  deleteSetting: (id) => {
    const url = `/settings/destroy/${id}`;
    return axiosClient.delete(url);
  },

  createSetting: (data) => {
    const url = '/settings/store';
    return axiosClient.post(url, data );
  },

  updateSetting: (id,data) => {
    const url = `/settings/update/${id}`;
    return axiosClient.post(url, data );
  },

  getSetting: (id) => {
    const url = `/settings/show/${id}`;
    return axiosClient.get(url);
  },
}

export default settingApi;