import axiosClient from "./axiosClient";

const marketApi = {
  sellHost: (params) => {
    const url = '/api/HorseMarket/SellHorse';
    return axiosClient.post(url, { params });
  },

  auctionHorse: (params) => {
    const url = '/api/HorseMarket/AuctionHorse';
    return axiosClient.post(url, { params });
  },

  buyHorse: (params) => {
    const url = '/api/HorseMarket/BuyHorse';
    return axiosClient.post(url, { params });
  },
  
  horseList: (params) => {
    const url = '/api/HorseMarket/HorseList';
    return axiosClient.get(url, { params });
  }
}

export default marketApi;