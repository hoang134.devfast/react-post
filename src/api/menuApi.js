import axiosClient from "./axiosClient";

const menuApi = {
  getAll: (params) => {
    const url = '/menus';
    return axiosClient.get(url, { params });
  },

  deleteMenu: (id) => {
    const url = `/menus/destroy/${id}`;
    return axiosClient.delete(url);
  },

  createMenu: (data) => {
    const url = '/menus/store';
    return axiosClient.post(url, data );
  },

  updateMenu: (id,data) => {
    const url = `/menus/update/${id}`;
    return axiosClient.put(url, data );
  },

  updateStatusMenu: (id,data) => {
    const url = `/menus/updateStatus/${id}`;
    return axiosClient.put(url, data );
  },

  getMenu: (id) => {
    const url = `/menus/show/${id}`;
    return axiosClient.get(url );
  },
}

export default menuApi;