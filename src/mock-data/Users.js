const Users = [
    {
        id: "abc1",
        name: "demo1",
        email: "email1",
        status: 1 // high
    },
    {
        id: "abc2",
        name: "demo12",
        email: "email12",
        status: 0 // low
    },
    {
        id: "abc3",
        name: "demo13",
        email: "email13",
        status: 1 // medium
    },
    {
        id: "abc4",
        name: "demo14",
        email: "email14",
        status: 0 // low
    },
    {
        id: "abc5",
        name: "demo15",
        email: "email15",
        status: 1 // high
    },
    {
        id: "abc6",
        name: "demo16",
        email: "email16",
        status: 1 // medium
    }
];

export default Users;