export const MenuDashboard = [{
    id: "1",
    name: "ユーザー数の合計",
    link: "/",
    icon: "tachometer-alt"
}]

export const MenuSystem =
[
    {
        id: "3",
        name: "カテゴリ",
        link: "/category",
        icon: "clipboard"
    },
    {
        id: "7",
        name: "設定",
        link: "/settings",
        icon: "cogs"
    }
]

export const MenuModule =
[
        {
            id: "2",
            name: "ユーザー",
            link: "/user",
            icon: "users"
        },
        {
            id: "6",
            name: "メニュー",
            link: "/menu",
            icon: "caret-square-down"
        },    
        {
            id: "5",
            name: "新聞",
            link: "/posts",
            icon: "passport"
        }
    
    ]
