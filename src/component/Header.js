import { Dropdown } from 'react-bootstrap'
import styles from '../styles/Home.module.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars, faSignOutAlt, faUserCircle, faKey } from '@fortawesome/free-solid-svg-icons'
import React from 'react'
//import Link from 'next/link'
//import CheetahAxios from '../cheetah-core/cheetah-axios/cheetah-axios'
//import LogoutApi from '../api/logoutApi'
import * as UserLoginInfo from '../cheetah-core/mixins/user-login-info'
import * as lang from "../locales/ja-JP.json";

class Header extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            userLogin: {},
            isActive: false
        }
    }

    componentDidMount() {
        /**
         * get info User login
         */
        const inforUser = UserLoginInfo.getUserLogin()
        this.setState({userLogin: inforUser})
    }

    clearUser(params) {
        
    }

    /**
     * logout user
     */
    async Logout () {
        // const cheetah = new CheetahAxios()
        // try {
        //     await LogoutApi.logout().then((res) => {
        //             cheetah.clearAuthToken()
        //             UserLoginInfo.removeUserLogin()
        //     })
        //     window.location.href = "/login"
        //   } catch (error) {
        //     console.log(error);
        //   }
    }

    onClickIconMenu () {
        this.props.onClickIconMenu()
    }
    
    render() {
        return (
            <div className="d-flex flex-wrap mr-0 ml-0 h-100 justify-content-between">
                <div className=" border-end" style={{width: 250 + 'px'}}>
                    <div className="d-flex align-items-center h-100 ps-2">
                        <div className={styles.logo + ' text-nowrap overflow-hidden me-3'}>
                            <a href="/">
                                <a className={styles.logoActive}>
                                    <img src="/logo.png" alt="Logo" className="me-2"/>
                                    <span className={`vertical-align-middle ml-1`}>Cheetah-React</span>
                                </a>
                            </a>
                        </div>
                    
                        <div className={styles.sidebarHide}>
                            <FontAwesomeIcon icon={faBars} onClick={() => this.onClickIconMenu()}/>
                        </div>
                    </div>
                </div>
                { this.state.userLogin && 
                    <div className="col-auto d-flex align-items-center h-100">
                        <Dropdown>
                            <Dropdown.Toggle variant="white" id="dropdown-basic">
                                Hello 
                                <span className="ms-1 me-2">{this.state.userLogin.name}</span>
                                <span className={styles.imgAvatar}></span>
                            </Dropdown.Toggle>
                            <Dropdown.Menu id={styles.dropMenu}>
                                <Dropdown.Item href={`/user/${this.state.userLogin.id}`}  className="border-bottom">
                                    <span>
                                    <FontAwesomeIcon icon={faUserCircle} className="align-middle me-2" style={{height:'15px'}}/>  
                                    </span>  
                                    { lang.common.profile}
                                </Dropdown.Item>
                                <Dropdown.Item href={`/user/changePassword/${this.state.userLogin.id}`}>
                                    <span>
                                    <FontAwesomeIcon icon={faKey} className="align-middle me-2" style={{height:'15px'}}/>
                                    </span>
                                    { lang.common.change_password}
                                </Dropdown.Item>
                                <Dropdown.Item as="button" onClick={() => this.Logout()}>
                                    <FontAwesomeIcon icon={faSignOutAlt} className="align-middle me-2" style={{height:'15px'}}/> 
                                    { lang.common.logout}
                                </Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>
                    </div>
                }
            </div>
        )
    }
}

export default Header