import styles from '../styles/Home.module.css'
const Footer = () => (
    <div className={styles.footer}>
    <div className="d-flex">
      &copy; 2021
      <a href="http://devfast.us/" target="_blank" rel="noreferrer" className={styles.footerLink}>Compare Item</a>
    </div>

    <div className="d-flex">
      Powered by
      <a href="http://devfast.us/" target="_blank" rel="noreferrer" className={styles.footerLink}>DevFast On Cheetah Platform</a>
    </div>
  </div>
)

export default Footer