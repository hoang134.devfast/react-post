
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import {MenuSystem, MenuModule, MenuDashboard} from '../../mock-data/Menu'
import styles from '../../styles/Home.module.css'
//import Link from 'next/link'
import globalCss from "../../assets/css/style.module.css";
import { useEffect, useState } from 'react'

function NavBarMenu ({props, onClickButtonBottom}) {

    useEffect(() => {
        console.log(props);
    }, [props]);
    function onClickButton() {
        onClickButtonBottom()
    }

    return (
        <div className={`${styles.sideBar} ${props && props.isActive? styles.sideBarAfterClick : ''} ${props && props.onHideMenu? styles.sideBarAfterClickMenu : ''} `}>
            <ul className={styles.sidebarNav}>
                {MenuDashboard.map((menu,key)=> 
                    (
                        <a key={key} href={menu.link} passHref >
                            <li className={styles.navItem + ' text-wrap overflow-hidden'}>
                                <a className={styles.navLink}>
                                    <FontAwesomeIcon icon={menu.icon}  className={`${globalCss.icon} ${props && props.isActive? 'text-center': ''} ` } />
                                    <span className={props && props.isActive? 'd-none': ''}>{menu.name}</span>
                                </a>
                            </li>
                        </a>
                    ))}
            </ul>
            <ul className={styles.sidebarNav}>
                <li className={props && props.isActive? 'd-none': '' + " text-primary fw-bold my-2 " + styles.title}>システム</li>
                {MenuSystem.map((menu,key)=> 
                    (
                        <a key={key} href={menu.link} passHref >
                            <li className={styles.navItem + ' text-wrap overflow-hidden'}>
                                <a className={styles.navLink}>
                                    <FontAwesomeIcon icon={menu.icon}  className={`${globalCss.icon} ${props && props.isActive? 'text-center': ''} ` } />
                                    <span className={props && props.isActive? 'd-none': ''}>{menu.name}</span>
                                </a>
                            </li>
                        </a>
                    ))}
            </ul>
            <ul className={styles.sidebarNav}>
                <li className={props && props.isActive? 'd-none': ''+" text-primary fw-bold my-2 " + styles.title}>モジュール</li>
                {MenuModule.map((menu,key)=> 
                    (
                        <a key={key} href={menu.link} passHref >
                            <li className={styles.navItem + ' text-wrap overflow-hidden'}>
                                <a className={styles.navLink}>
                                    <FontAwesomeIcon icon={menu.icon} className={`${globalCss.icon} ${props && props.isActive? 'text-center': ''} ` } />
                                    <span className={props && props.isActive? 'd-none': ''}>{menu.name}</span>
                                </a>
                            </li>
                        </a>
                    ))}
            </ul>
            <div className={styles.sidebarTrigger} onClick={() => onClickButton()}>
                <FontAwesomeIcon className={props && props.isActive? 'd-none': 'd-block'} icon="chevron-left" />
                <FontAwesomeIcon className={props && props.isActive? 'd-block': 'd-none'} icon="chevron-right" />
            </div>
        </div>
     )
}

export default NavBarMenu