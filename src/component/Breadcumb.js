//import { useRouter } from 'next/dist/client/router'
import { useEffect,useState } from 'react'
import styles from '../styles/Home.module.css'
import { BREAD_CRUMB_LIST } from "../constants/index";
import * as t from "../locales/ja-JP.json";

const Breadcumb = () =>{
    //const router = useRouter();
    const [breadcrumb, setBreadcrumb] = useState('');

    useEffect(()=>{
        //setBreadcrumb(router.pathname.split('/').filter((it)=>it)[0])
    },[])

    /**
     * Convert name to Japan
     * 
     * @param {*} string 
     * @returns 
     */
    const convert =(string)=> {
        const name =  BREAD_CRUMB_LIST.find((element) => { 
            return element.value == string
         }).name
    const toArray = name.split(".")
    if (t[toArray[0]][toArray[1]]) {
        return t[toArray[0]][toArray[1]]
    }
    return 'Home'
    }

    return  (
        <div id={styles.breadCumb}>
            <nav aria-label="breadcrumb">
                <ol className={styles.breadcumbMain}>
                    <li className={styles.breadcumbItem}>
                        <a href="">{breadcrumb && (convert(breadcrumb))}</a>
                    </li>
                </ol>
            </nav>
        </div>
      )
}

export default Breadcumb