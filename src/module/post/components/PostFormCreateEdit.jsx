import Head from 'next/head'
import categoryApi from '../../../api/categoryApi';
import postApi from '../../../api/postApi';
import { useEffect, useState, useRef  } from 'react'
import Select from 'react-select'
import ImageUploading from 'react-images-uploading';
import React from 'react';
// import { useRouter } from 'next/router'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import globalCss from '../../../assets/css/style.module.css'
//import Link from 'next/dist/client/link';
import { Col, Form, Row, Button } from 'react-bootstrap'
// import { CKEditor } from '@ckeditor/ckeditor5-react';
// import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

export default function PostCreateEdit({listCate}) {
    const [post, setPost] = useState([]);
    const [options, setOptions] = useState([]);
    const [fileImage, setFileImage] = useState(null);
    const [curentImage, setCurentImage] = useState(null);
    const [content, setContent] = useState(null);
    const [curnentCategory, setCurnentCategory] = useState({label:'', value: 0});

  
    //const router = useRouter()
    const checkNumber = (number) => {
        if (isNaN(number) === false) {
            return number
        }
        return
     }
     const id = 1
    //ckeditor
    const editorRef = useRef()
    const [editorLoaded, setEditorLoaded] = useState(false)
    const { CKEditor, ClassicEditor } = editorRef.current || {}
    const getPost = async (id) => {
        try {
           const response = await postApi.getPost(id);
           setPost(response.data)
           setCurentImage(response.data.img)
        } catch (error) {
            console.log(error)
        }
    }
  const getCurentCategory = (id) => {
    if (listCate == undefined) {
          return curnentCategory
    }
    
    if(listCate.length > 0)
    {
        const cate = listCate.find((cate)=> {
           return cate.value == id
           })
        if(curnentCategory.value == 0)
        {
            setCurnentCategory(cate)
        }
    }
    return curnentCategory
  } 
    useEffect(() => {
        editorRef.current = {
            // CKEditor: require('@ckeditor/ckeditor5-react'), // depricated in v3
            CKEditor: require('@ckeditor/ckeditor5-react').CKEditor, // v3+
            ClassicEditor: require('@ckeditor/ckeditor5-build-classic')
          }
          getListCategory()
          if(id != undefined && id)
          {
              getPost(id)
              setEditorLoaded(true)
          }
          
          if(listCate == undefined)
          {
              setEditorLoaded(true)
          }
       
    }, [id, listCate]);

    /**
     * Get category
     */
    const getListCategory = async () => {
        try {
           const response = await categoryApi.getAll();
            const listCategory = response.data.map((cate)=> {
                return {'value': cate.id, 'label': cate.name }
            })
           setOptions(listCategory)
        } catch (error) {
            console.log(error)
        }
    }
    
    /**
     * Submit form
     * 
     * @param {*} event 
     */
    const handleSubmit = async event => {
        event.preventDefault()
        var bodyFormData = new FormData();
        bodyFormData.append('category_id',  event.target.category_id.value);
        bodyFormData.append('title', event.target.title.value);
        bodyFormData.append('content', content);
        bodyFormData.append('viewed', event.target.viewed.value);
        bodyFormData.append('public_start_at', event.target.public_start_at.value);
        bodyFormData.append('public_end_at', event.target.public_end_at.value);
        bodyFormData.append('description', event.target.description.value);
        bodyFormData.append('status', event.target.status.value);
        if(fileImage != null)
        {
           bodyFormData.append('img',fileImage); 
        }
       
        try {
            if(id != undefined)
            { 
                const response = await postApi.updatePost(id, bodyFormData);
            }
            else {
                const response = await postApi.createPost(bodyFormData);
            }
            //router.push('/posts')
         } catch (error) {
             console.log(error)
         }
      }
      const [images, setImages] = React.useState([]);
      const maxNumber = 69;

      /**
       * 
       * @param {*} imageList 
       * @param {*} event 
       */
      const onChange = (imageList, event) => {
        // data for submit
        setFileImage(imageList[imageList.length-1].file)
        setImages(imageList);
      };

      const statusActive = {
        'margin-left': '30px',
      };
      const borderRight = {
        'border-right': '1px solid #dee2e6',
      };
      const formLabel = {
        'display':' flex',
        'flex-wrap': 'wrap',
        'margin-right':' -5px',
        'margin-left': '-5px',
      };
      const inputRight = {
          'width':'85%',
          'display': 'inline'
      }
     const handleChange = (value) => {
        setCurnentCategory(value)
    }


    const colourStyles = {
        control: styles => ({ ...styles, backgroundColor: 'white',  width:'100%'}),
      };

      const defauImg = {
        'border': '3px dashed #00000057 ',
        'height': '180px',
        'width': '180px',
      }
      const onCancel = () => {
        //router.push('/posts')
      }
    return editorLoaded ? (
        <>
            <Head>
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous" />
            </Head>
            <form onSubmit={handleSubmit} className="">
                <div className="row p-2" >
                    <div style={borderRight} className="col-8 p-3">
                        <div className="form-group">
                            
                            <label htmlFor="title" className="mb-2">
                                記事のタイトル
                            </label>
                            <input type="text" defaultValue={post.title} className="form-control" id="title" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="content" className="mb-2 mt-2">
                                備考欄
                            </label>
                            <textarea rows="5"  name="description" type="text"  defaultValue={post.description} className="form-control" id="description"></textarea>
                        </div>
                        <div className="form-group">
                            <label htmlFor="content" className="mb-2 mt-2">
                            備考欄
                            </label>
                            <div className="App">
                                 <CKEditor
                                 editor={ ClassicEditor }
                                 data={post.content}
                                 onReady={ editor => {
                                     // You can store the "editor" and use when it is needed.
                                     console.log( 'Editor is ready to use!', editor );
                                 } }
                                 onChange={ ( event, editor ) => {
                                     const data = editor.getData();
                                     setContent(data)
                                     console.log( { event, editor, data } );
                                 } }
                                 onBlur={ ( event, editor ) => {
                                     console.log( 'Blur.', editor );
                                 } }
                                 onFocus={ ( event, editor ) => {
                                     console.log( 'Focus.', editor );
                                 } }
                             />
                            </div>
                        </div>
                    </div>
                
                    <div className="col-4 p-3">
                        <div className="form-row form-group" style={formLabel}>
                            <label htmlFor="exampleInputEmail1" className="col-md-4 col-form-label d-flex justify-content-end me-3">
                                <p>カテゴリ</p>
                            </label>
                            <div className="col-md-8 col-lg-7">
                                <Select styles={colourStyles} onChange={value => handleChange(value)} value={getCurentCategory(post.category_id) } name="category_id" options={options} />
                            </div>
                        </div>
                        <div className="form-row form-group" style={formLabel}>
                            <label htmlFor="viewed" className="col-md-4 col-form-label d-flex justify-content-end me-3">
                                <p>閲覧</p>
                            </label>
                            <div className="col-md-8 col-lg-7">
                                {/* <input style={inputRight} type="number" defaultValue={post.viewed} className="form-control" id="viewed"/> */}
                                <div className="input-group mb-3">
                                    <span className="input-group-text" id="basic-addon1"><FontAwesomeIcon icon="plus" style={{height:'15px'}}/></span>
                                    <input type="number" className="form-control" defaultValue={post.viewed} id="viewed" />
                                </div>
                            </div>
                        </div>

                        <div className="form-row form-group" style={formLabel}>
                            <label htmlFor="public_start_at" className="col-md-4 col-form-label d-flex justify-content-end me-3">
                                <p>開始時間</p>
                            </label>
                            <div className="col-md-8 col-lg-7">
                                {/* <input type="date" defaultValue={post.public_start_at} style={inputRight} className="form-control" id="public_start_at"/> */}
                                <div className="input-group mb-3">
                                    <span className="input-group-text" id="basic-addon1"><FontAwesomeIcon icon="map-marker" style={{height:'15px'}}/></span>
                                    <input type="date" defaultValue={post.public_start_at} className="form-control" id="public_start_at"/>
                                </div>
                            </div>
                        </div>
                        
                        <div className="form-row form-group" style={formLabel}>
                            <label htmlFor="public_end_at"  className="col-md-4 col-form-label d-flex justify-content-end me-3">
                               <p>終了時間</p>
                            </label>
                            <div className="col-md-8 col-lg-7">
                                {/* <input type="date" defaultValue={post.public_end_at} style={inputRight} className="form-control" id="public_end_at"/> */}
                                <div className="input-group mb-3">
                                    <span className="input-group-text" id="basic-addon1"><FontAwesomeIcon icon="map-marker" style={{height:'15px'}}/></span>
                                    <input type="date" defaultValue={post.public_end_at} className="form-control" id="public_end_at"/>
                                </div>
                            </div>
                        </div>
                        
                        <div className="d-flex">
                            <label htmlFor="public_end_at"  className=" p-0 col-md-4 col-form-label d-flex justify-content-end me-3">
                                <p className="p-0 "> ステータス</p>
                            </label>
                            <div className="col-md-8 col-lg-7 row">
                                 <div className="d-flex">
                                    <div className="form-check">
                                        {post.status === 1 ?   
                                        <input className="form-check-input" value="1"  type="radio" name="status" id="flexRadioDefault2" defaultChecked= "true" />
                                        :<input  className="form-check-input" value="1"  type="radio" name="status" id="flexRadioDefault2" />}
                                        <label className="form-check-label " htmlFor="flexRadioDefault2">
                                        有効
                                        </label>
                                    </div>
                                
                                    <div style={statusActive} className="form-check">
                                        <input className="form-check-input" value="0" type="radio" name="status" id="flexRadioDefault1" defaultChecked= "true"/>
                                        <label className="form-check-label" htmlFor="flexRadioDefault1">
                                        無効
                                        </label>
                                    </div>
                                 </div>
                            </div>
                        </div>

                        <div className="form-row form-group" style={formLabel}>
                            <label htmlFor="viewed" className="col-md-4 col-form-label d-flex justify-content-end me-3">
                                <p>閲覧</p>
                            </label>
                            <div className="App col-md-8 col-lg-7">
                                <ImageUploading
                                    multiple
                                    value={images}
                                    onChange={onChange}
                                    maxNumber={maxNumber}
                                    dataURLKey="data_url"
                                >
                                    {({
                                    imageList,
                                    onImageUpload,
                                    onImageRemoveAll,
                                    onImageUpdate,
                                    onImageRemove,
                                    isDragging,
                                    dragProps,
                                    }) => (
                                    // write your building UI
                                    <div className="upload__image-wrapper">
                                            <div className="image-item">
                                                <img onClick={onImageUpload} src={imageList[0]? imageList[imageList.length-1]['data_url']: curentImage} alt="" width="85%" />
                                                <div className="image-item__btn-wrapper">
                                                </div>
                                            </div>
                                        {imageList[0]?
                                        ''
                                            : 
                                                post.img != null?  '' 
                                                : <div onClick={onImageUpload} style= {defauImg} className="d-flex">
                                                    <span className="m-auto"> File image</span>
                                                 </div> 
                                            
                                        }
                                        {/* <a
                                        className="btn btn-outline-info mt-2"
                                            style={isDragging ? { color: 'red' } : undefined}
                                            onClick={onImageUpload}
                                            {...dragProps}
                                            >
                                            File image
                                        </a> */}
                                    </div>
                                    )}
                                </ImageUploading>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="d-flex card-footer bg-white text-center mt-3 pt-3">
                    <div className="m-auto">
                        <Button 
                            variant="outline-primary"
                            type="submit"
                            className={"bg-white-for-btn-outline me-4 " + globalCss.btnSearch}
                            >
                                <FontAwesomeIcon icon="save" className={globalCss.icon} size="xs" />保存
                        </Button>
                        <Button
                            variant="outline-danger"
                            className={"bg-white-for-btn-outline " + globalCss.btnCancel}
                            onClick={onCancel}
                        >
                            <FontAwesomeIcon icon="arrow-left" className={globalCss.icon} size="xs" />
                            キャンセル
                        </Button>
                    </div>
                </div>
            </form>
        </>
    ): (
            <div>Editor loading</div>
          )
}