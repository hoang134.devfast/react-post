import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { style } from 'dom-helpers'
import Head from 'next/head'
import Select from 'react-select'
import {useState} from 'react'
import globalCss from "../../../assets/css/style.module.css";

export default function PostFormSearch({onSearch, listCategory}) {

    const [categorId, setCategorId] = useState(null);
    const [title, setTitle] = useState([]);
    const [publicStartAt, setPublicStartAt] = useState([]);
    const [publicEndAt, setPublicEndAt] = useState([]);

    /**
     * submit Form
     * 
     * @param {*} event 
     */
    const handleSubmit = (event) => {
        event.preventDefault()
        let conditions = {
            category_id:  event.target.category_id.value,
            title:  event.target.title.value,
            public_start_at:  event.target.public_start_at.value,
            public_end_at:  event.target.public_end_at.value,
            status:  event.target.status.value,
        }
        onSearch(conditions)
    }

    const statusActive = {
        'margin-left': '30px',
      };
    const options =  listCategory

    /**
     * Clear form
     */
    const clearForm = () => {
     setTitle('')
     setPublicEndAt('')
     setPublicEndAt('')
     setCategorId({value:"", label:""})
    }
    return (
        <>
            <Head>
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous" />
            </Head>
            <h5 className="cursor-pointer user-select-none">
                検索条件
            </h5>
            <form onSubmit={handleSubmit}>
                <div className="d-flex">
                    <div className="form-check">
                        <input value="1" className="form-check-input" type="radio" name="status" id="flexRadioDefault2" checked/>
                        <label className="form-check-label" htmlFor="flexRadioDefault2">
                        有効
                        </label>
                    </div>

                    <div style={statusActive} className="form-check">
                        <input className="form-check-input" value="0" type="radio" name="status" id="flexRadioDefault1"/>
                        <label className="form-check-label" htmlFor="flexRadioDefault1">
                        無効
                        </label>
                    </div>
                </div>

                <div className="row">
                    <div className="col-6">
                        <div className="form-group">
                            <label  className="mb-2 mt-2" htmlFor="title">記事のタイトル</label>
                            {/* <input value={title}  onChange={(e)=>setTitle(e.target.value)}  type="text"  className="form-control" id="title" /> */}
                            <div className="input-group mb-3">
                                <span className="input-group-text" id="basic-addon1"><FontAwesomeIcon icon="eraser" style={{height:'15px'}}/></span>
                                <input  value={title}  onChange={(e)=>setTitle(e.target.value)}  type="text"  className="form-control" id="title"/>
                            </div>
                        </div>

                        <div className="form-group">
                            <label className="mb-2 mt-2" htmlFor="public_start_at">開始時間</label>
                            {/* <input value={publicStartAt} onChange={(e)=>setPublicStartAt(e.target.value)} type="date" className="form-control" id="public_start_at"/> */}
                            <div className="input-group mb-3">
                                <span className="input-group-text" id="basic-addon1"><FontAwesomeIcon icon="map-marker" style={{height:'15px'}}/></span>
                                <input value={publicStartAt} onChange={(e)=>setPublicStartAt(e.target.value)} type="date" className="form-control" id="public_start_at"/>
                            </div>
                        </div>
                    </div>

                    <div className="col-6">
                        <div className="form-group mb-3">
                            <label className="mb-2 mt-2" htmlFor="exampleInputEmail1">カテゴリ</label>
                            <Select  value={categorId}  onChange={value =>setCategorId(value)} name="category_id" options={options} />
                        </div>

                        <div className="form-group">
                            <label  className="mb-2 mt-2" htmlFor="public_end_at">終了時間</label>
                            {/* <input  value={publicEndAt}  onChange={(e)=>setPublicEndAt(e.target.value)} type="date" className="form-control" id="public_end_at"/> */}
                            <div className="input-group mb-3">
                                <span className="input-group-text" id="basic-addon1"><FontAwesomeIcon icon="map-marker" style={{height:'15px'}}/></span>
                                <input value={publicEndAt}  onChange={(e)=>setPublicEndAt(e.target.value)} type="date" className="form-control" id="public_end_at"/>
                            </div>
                        </div>
                    </div>

                </div>
                <button type="submit" className={"btn btn-outline-primary mt-3 me-3 " + globalCss.btnSearch}>
                <FontAwesomeIcon icon="search" className={globalCss.icon} />
                検索</button>
                <a onClick={clearForm} className={"btn btn-outline-primary mt-3 " + globalCss.btnClear}>
                    <FontAwesomeIcon icon="eraser" className={globalCss.icon} />
                    クリア</a>
            </form>
        </>
    )
}