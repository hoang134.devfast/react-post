//import Head from 'next/head'
//import Link  from 'next/dist/client/link'
import { Col, Form, Button, InputGroup, Row, Table } from "react-bootstrap";
import SweetAlert from 'react-bootstrap-sweetalert'
import { useState } from 'react'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {badgeActive, badgeUnactive} from "../../../styles/Home.module.css"
//import PostFormSearch from './PostFormSearch';

export default function PostTable({props, onDelete, listCategory, updateStatus}) {
    const [showAlert, setShowAlert] = useState({onShow: false, id: null});
  function deletePost(id) {
    onDelete(id)
    setShowAlert({
        onShow: false,
        id: null
     })
    // props.onDelete(id)
  }
  const image = {
    width: '160px',
    height: 'auto',
    'object-fit': 'cover'
  };
  const updateStatusPost = (id, status) => {
      if (status == 0) {
          status = 1
      }
      else {
          status = 0
      }
    var bodyFormData = new FormData();
    bodyFormData.append('status', status);
    updateStatus(id, bodyFormData)
  }
  const getNameCategory = (id ) => {
    if(listCategory.length)
    {
        const cate = listCategory.find((cate)=> {
           return cate.value == id
           })
           if (cate) {
               return cate.label
           }
           return "None"
    }
  } 

  const textEllip = {
    'width': 'auto',
    'overflow': 'hidden',
    'text-overflow': 'ellipsis',
    '-webkit-line-clamp': '4',
    'height': 'auto',
    'display': '-webkit-box',
    '-webkit-box-orient': 'vertical',
    'margin':'10px'
  }
    return (
        <>
            <head>
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous" />
            </head>
            <Table responsive striped bordered outlined hover className="bg-white">
                <thead>
                    <tr>
                        <th scope="col" className="text-center">ID</th>
                        <th scope="col">画像</th>
                        <th scope="col">備考欄</th>
                        <th scope="col">カテゴリ</th>
                        <th scope="col" className="text-center" width="10%">ステータス</th>
                        <th scope="col" className="text-center" width="10%">閲覧数</th>
                        <th scope="col" className="text-center" style={{width: '120px'}}>アクション</th>
                    </tr>
                </thead>
                <tbody>
                    {props && props.map(function(post, idx){
                        return (
                        <tr key={idx}>
                            <td className="text-center">{post.id}</td>
                            <td>
                                <img  style={image} src={post.img} ></img>
                            </td> 
                            <td>
                                <div style={textEllip}>
                                    {post.title}
                                </div>
                                <div style={textEllip}>
                                {post.description}
                                </div>
                                </td> 
                            <td>{getNameCategory(post.category_id)}</td>
                            {/* <td>{post.status ==0? '無効' : '有効'}</td> */}
                            <td className="text-center" onClick={() => updateStatusPost(post.id, post.status)}>
                                            { post.status === 1 ?
                                            <span className={badgeActive}>有効</span>
                                            :
                                            <span className={badgeUnactive}>無効</span>
                                        }
                            </td>
                            <td className="text-center">{post.viewed}</td>
                            <td className="text-center">
                            {/* <Link href={`posts/${post.id}`} >
                                <a className="text-primary box-shadow-none over p-1 mx-1">Edit</a>
                            </Link>
                            <a className="text-primary box-shadow-none over p-1 mx-1"
                                onClick={(event) => setShowAlert({onShow: true, id: post.id})}>Delete</a>
                            <SweetAlert
                                show={showAlert.onShow}
                                title={`Are you sure delete ${post.name}`}
                                onConfirm={() => deletePost(post.id)}
                                onCancel={() => setShowAlert({
                                    onShow: false,
                                    id: null
                                    })}
                                btnSize="sm"
                                showCancel
                                warning
                                confirmBtnBsStyle="danger"
                                confirmBtnText="Yes, delete it!"
                                showCloseButton
                            /> */}

                                <Button variant="link" size="sm" className="text-primary box-shadow-none over p-1 mx-1" href={`/posts/${post.id}`}>
                                    <FontAwesomeIcon icon="pencil-alt" style={{height:'1em'}}/>
                                </Button>
                            
                                <Button variant="link" size="sm" className="text-primary box-shadow-none over p-1 mx-1">
                                    <FontAwesomeIcon icon="info" style={{height:'1em'}} />
                                </Button>
                                {/* <Button variant="link" size="sm" className="text-danger box-shadow-none over p-1 mx-1" onClick={() => {if(window.confirm('Delete the item?')){deletePost(post.id)};}}>
                                    <FontAwesomeIcon icon="trash-alt" style={{height:'1em'}}/>
                                </Button> */}

                                <Button variant="link" size="sm" className="text-danger box-shadow-none over p-1 mx-1" onClick={(event) => setShowAlert({onShow: true, id: post.id})}>
                                    <FontAwesomeIcon icon="trash-alt" style={{height:'1em'}}/>
                                </Button>

                                <SweetAlert
                                show={showAlert.onShow}
                                title={`Are you sure delete`}
                                onConfirm={() => deletePost(showAlert.id)}
                                onCancel={() => setShowAlert({
                                    onShow: false,
                                    id: null
                                    })}
                                btnSize="sm"
                                showCancel
                                warning
                                confirmBtnBsStyle="danger"
                                confirmBtnText="Yes, delete it!"
                                showCloseButton
                                />
                            </td>
                        </tr>
                        )
                    })}
                </tbody>
            </Table>
        </>
    )
}