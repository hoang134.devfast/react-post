import axiosClient from "../../../api/axiosClient";
const postApi = {
    getAll: (params) => {
      const url = '/posts';
      return axiosClient.get(url, { params });
    },
  
    deletePost: (id) => {
      const url = `/posts/destroy/${id}`;
      return axiosClient.delete(url);
    },
  
    createPost: (data) => {
      const url = '/posts/store';
      return axiosClient.post(url, data );
    },
  
    updatePost: (id,data) => {
      const url = `/posts/update/${id}`;
      return axiosClient.post(url, data );
    },
  
    updateStatus: (id, data) => {
      const url = `/posts/updateStatus/${id}`;
      return axiosClient.post(url, data);
    },
  
    getPost: (id) => {
      const url = `/posts/show/${id}`;
      return axiosClient.get(url );
    },
  }
  
  export default postApi;