import React from 'react';
import { Route, Routes, useLocation } from 'react-router-dom';
import NotFound from '../../component/NotFound';

import Index from './pages/index';

Post.propTypes = {};

function Post(props) {
 const match = useLocation ();
   console.log(match.pathname);
  return (
    <div>
      <Routes>
        <Route  path="" element={<Index/>} />
        <Route path="*" component={NotFound} />
    </Routes>
    </div>
  );
}

export default Post;
