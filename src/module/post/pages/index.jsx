import { useEffect, useState } from 'react'
import postApi from '../api/postApi';
import categoryApi from '../api/categoryApi';
import PostTable from '../components/PostTable';
import PostFormSearch from '../components/PostFormSearch';
import { Button } from 'react-bootstrap'
import style from '../../../assets/css/style.module.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {  toast } from 'react-toastify';

export default function Index() {
    const [posts, setPosts] = useState([]);
    const [categories, setCategories] = useState([]);

    /**
     * get Post
     * 
     * @param {*} conditions 
     */
    const getListPost = async (conditions) => {
        try {
           const response = await postApi.getAll(conditions);
           setPosts(response.data)
        } catch (error) {
            console.log(error)
        }
    }

    /**
     * Get category
     */
    const getListCategory = async () => {
        try {
           const response = await categoryApi.getAll();
          const listCategory = response.data.map((cate)=> {
                return {'value': cate.id, 'label': cate.name }
            })
         setCategories(listCategory)
        } catch (error) {
            console.log(error)
        }
    }

    /**
     * Update status
     * 
     * @param {*} id 
     * @param {*} data 
     */
    const updateStatus = async (id , data ) => {
        try {
           const response = await postApi.updateStatus(id, data);
           getListPost(null)
           toast.success('Success')
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        getListPost(null)
        getListCategory()
    }, []);

    /**
     * Delete post
     * 
     * @param {*} id 
     */
    const onDelete = async (id ) => {
        try {
           const response = await postApi.deletePost(id);
           getListPost(null)
           toast.success('Success')
        } catch (error) {
            console.log(error)
        }
    }

    /**
     * Search post
     * 
     * @param {*} conditions 
     */
    const onSearch = (conditions ) => {
        getListPost(conditions)
    }
    return (
        <>
            <PostFormSearch onSearch={onSearch} listCategory={categories}></PostFormSearch>
                <div className="d-flex justify-content-end mb-3">
                    <Button size="sm" href="/posts/create">
                        <FontAwesomeIcon icon="plus" className={style.icon + ' m-0 ' + style.iconAction} />
                    </Button>
                </div>
            <PostTable props= {posts} onDelete={onDelete} listCategory={categories} updateStatus={updateStatus}>
            </PostTable>
        </>
    )
}