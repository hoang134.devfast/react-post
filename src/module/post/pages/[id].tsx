import PostCreateEdit from '../components/PostFormCreateEdit';
import { useEffect, useState } from 'react'
import categoryApi from '../../../api/categoryApi';

export default function CreateEditPost() {
    const [categories, setCategories] = useState([]);

    /**
     * Get category
     */
    const getListCategory = async () => {
        try {
            const response = await categoryApi.getAll();
            const listCategory = response.data.map((cate:any)=> {
                return {'value': cate.id, 'label': cate.name }
            })
            setCategories(listCategory)
        } catch (error) {
            console.log(error)
        }
    }
    useEffect(() => {
        getListCategory()
    }, []);

    return (
        <>
        <div className="bg-white">
            <h5 className="card-header p-3 bg-white text-primary">投稿</h5>
            <div className="p-3">
                <PostCreateEdit listCate={categories}></PostCreateEdit>
            </div>
        </div>
        </>
    )
}